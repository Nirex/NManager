﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Input;
using NDC.NDynamics.Arguments;

namespace NManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Members
        private string CurrentDirectory;
        private bool bIsPaused;
        private int CurrentCommand;
        private int OpacityMod;
        private NDC.NDynamics.Core.AsyncWorker OpacityWorker = new NDC.NDynamics.Core.AsyncWorker(10);
        #endregion

        #region Default
        void PlaceHolderMethod() { }
        public MainWindow()
        {
            InitializeComponent();
            InitializeEvents();
            InitializeControls();

            Opacity = 0;
            OpacityMod = 0;

            CurrentDirectory = "C:\\";
            bIsPaused = false;

            GetContent(CurrentDirectory);
        }
        #endregion

        #region Initialization
        public void InitializeEvents()
        {
            KeyDown += MainWindow_KeyDown;

            BtnExit.Click += BtnExit_Click;
            BtnAbout.Click += BtnAbout_Click;

            BtnAddFile.Click += BtnAddFile_Click;
            BtnAddFolder.Click += BtnAddFolder_Click;
            BtnDelete.Click += BtnDelete_Click;
            BtnRename.Click += BtnRename_Click;

            MouseDown += MainWindow_MouseDown;

            OpacityWorker.WorkerInterval += OpacityWorker_WorkerInterval;
            OpacityWorker.RunAsyncWorker(PlaceHolderMethod);
        }
        private void InitializeControls()
        {

            NDC.NStyle.Container.Colors.Deep();

            TBCommand.Visibility = Visibility.Hidden;
            PHCommand.Visibility = Visibility.Hidden;

            BtnAddFile.Update();
            BtnAddFolder.Update();
            BtnDelete.Update();
            BtnRename.Update();

            ListContentList.Update();
            TBCommand.Update();
            PHCommand.Update();

            TBCommand.NLBL_Reference = PHCommand;
            PHCommand.NTB_Reference = TBCommand;

            LblTitleBar.Update();
            BtnExit.Update();
            BtnAbout.Update();
            LblStatus.Update();
        }
        #endregion

        #region Events
        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && bIsPaused)
            {
                switch (CurrentCommand)
                {
                    // Add File
                    case 1:
                        {
                            File.Create(CurrentDirectory + TBCommand.Text);
                            UpdateStatus("File created");
                            break;
                        }
                    // Add Folder
                    case 2:
                        {
                            Directory.CreateDirectory(CurrentDirectory + TBCommand.Text);
                            UpdateStatus("Directory created");
                            break;
                        }
                    // Delete
                    case 3:
                        {
                            if (TBCommand.Text.ToLower().Trim().TrimEnd().TrimStart() == "yes")
                            {
                                // Delete is confirmed
                                bool isDir = ((ListContentList.Get(ListContentList.SelectedIndex)) as NDC.NStyle.Controls.NSListBoxItem).Content.ToString().Contains("DIR");

                                if (isDir)
                                {
                                    Directory.Delete(((ListContentList.Get(ListContentList.SelectedIndex)) as NDC.NStyle.Controls.NSListBoxItem).Data, true);
                                    UpdateStatus("Directory deleted");
                                }
                                else
                                {
                                    File.Delete(((ListContentList.Get(ListContentList.SelectedIndex)) as NDC.NStyle.Controls.NSListBoxItem).Data);
                                    UpdateStatus("File deleted");
                                }
                            }

                            break;
                        }
                    // Rename
                    case 4:
                        {
                            bool isDir = ((ListContentList.Get(ListContentList.SelectedIndex)) as NDC.NStyle.Controls.NSListBoxItem).Content.ToString().Contains("DIR");

                            if (isDir)
                            {
                                Directory.Move(((ListContentList.Get(ListContentList.SelectedIndex)) as NDC.NStyle.Controls.NSListBoxItem).Data, CurrentDirectory + "//" + TBCommand.Text);
                                UpdateStatus("Directory renamed");
                            }
                            else
                            {
                                File.Move(((ListContentList.Get(ListContentList.SelectedIndex)) as NDC.NStyle.Controls.NSListBoxItem).Data, CurrentDirectory + "//" + TBCommand.Text);
                                UpdateStatus("File renamed");
                            }
                            break;
                        }
                    default:
                        break;
                }
                GetContent(CurrentDirectory);
                bIsPaused = false;
                HideCommand();
            }
            else
            {
                // Do Nothing
            }
        }  
        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            OpacityMod = 1;
        }
        private void BtnAbout_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://github.com/nirex0");
        }
        private void BtnAddFile_Click(object sender, RoutedEventArgs e)
        {
            bIsPaused = true;
            CurrentCommand = 1;
            ShowCommand("File Name...");
        }
        private void BtnAddFolder_Click(object sender, RoutedEventArgs e)
        {
            bIsPaused = true;
            CurrentCommand = 2;
            ShowCommand("Directory Name...");
        }
        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            bIsPaused = true;
            CurrentCommand = 3;
            ShowCommand("Yes/No...");
        }
        private void BtnRename_Click(object sender, RoutedEventArgs e)
        {
            bIsPaused = true;
            CurrentCommand = 4;
            ShowCommand("New File Name...");
        }
        private void BtnOpen_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start(((ListContentList.Get(ListContentList.SelectedIndex)) as NDC.NStyle.Controls.NSListBoxItem).Data);
        }
        private void OpacityWorker_WorkerInterval(object sender, AsyncWorkerArgs e)
        {
            if (OpacityMod == 0)
            {
                Opacity += 0.02;
                if (Opacity >= 1)
                {
                    Opacity = 1;
                }
            }
            else if (OpacityMod == 1)
            {
                Opacity -= 0.02;
                if (Opacity <= 0)
                {
                    // Exit
                    Application.Current.Shutdown();
                    Opacity = 0;
                }
            }
        }
        private void MainWindow_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if(e.LeftButton == MouseButtonState.Pressed)
            {
                try
                {
                    DragMove();
                }
                catch { }
            }
        }
        #endregion

        #region ListBox Functions
        void GetContent(string path)
        {
            try
            {
                ListContentList.Items.Clear();

                if (CurrentDirectory != "C:\\")
                {
                    ListContentList.Add("[DIR]: " + ".");
                    ListContentList.Get(ListContentList.Items.Count - 1).Data = CurrentDirectory + "\\.";
                    ListContentList.Get(ListContentList.Items.Count - 1).MouseDoubleClick += Directory_MouseDoubleClick;

                    ListContentList.Add("[DIR]: " + "..");
                    ListContentList.Get(ListContentList.Items.Count - 1).Data = CurrentDirectory + "\\..";
                    ListContentList.Get(ListContentList.Items.Count - 1).MouseDoubleClick += Directory_MouseDoubleClick;
                }

                foreach (string dir in Directory.GetDirectories(path))
                {
                    ListContentList.Add("[DIR]: " + System.IO.Path.GetFileName(dir));
                    ListContentList.Get(ListContentList.Items.Count - 1).Data = dir;
                    ListContentList.Get(ListContentList.Items.Count - 1).MouseDoubleClick += Directory_MouseDoubleClick;


                }
                foreach (string file in Directory.GetFiles(path))
                {
                    ListContentList.Add("[FILE]: " + System.IO.Path.GetFileName(file));
                    ListContentList.Get(ListContentList.Items.Count - 1).Data = file;
                    ListContentList.Get(ListContentList.Items.Count - 1).MouseDoubleClick += File_MouseDoubleClick;
                }
            }

            catch
            {
            }
        }
        private void Directory_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            CurrentDirectory = (sender as NDC.NStyle.Controls.NSListBoxItem).Data + "\\";
            GetContent(CurrentDirectory);
        }
        private void File_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            System.Diagnostics.Process.Start((sender as NDC.NStyle.Controls.NSListBoxItem).Data);
        }
        #endregion
        
        #region Commands
        void ShowCommand(string Holder)
        {
            TBCommand.Visibility = Visibility.Visible;
            PHCommand.Visibility = Visibility.Visible;
            TBCommand.Text = "";
            PHCommand.Content = Holder;
            TBCommand.Focus();
        }
        void HideCommand()
        {
            TBCommand.Visibility = Visibility.Hidden;
            PHCommand.Visibility = Visibility.Hidden;
            TBCommand.Text = "";
            PHCommand.Content = "";
        }
        #endregion
        
        #region Status
        void UpdateStatus(string value)
        {
            NDC.NStyle.Transition.Sharp.Label.Foreground(LblStatus,
                NDC.NStyle.Container.Colors.TEAL_DARK,
                NDC.NStyle.Container.Colors.TEAL_GLOW,
                250);
            LblStatus.Content = ("Status: " + value + " (Current Directory: \"" + CurrentDirectory + "\")");
        }
        #endregion
    }
}